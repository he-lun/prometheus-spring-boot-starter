//package top.codef.codef.properties;
//
//import org.springframework.boot.context.properties.ConfigurationProperties;
//
//import feign.Logger.Level;
//
//@ConfigurationProperties(prefix = "prometheus.codef")
//public class PrometheusCodefCloudProperties {
//
//	/**
//	 * 开启codef通知服务
//	 */
//	private boolean enabled = false;
//
//	/**
//	 * codef的api地址
//	 */
//	private String url = "https://api.codef.top";
//
//	/**
//	 * codef的tenantId
//	 */
//	private String tenantId;
//
//	/**
//	 * codef 的accessId
//	 */
//	private String accessId;
//
//	/**
//	 * codef的accesssecret
//	 */
//	private String accessSecret;
//
//	/**
//	 * 在codef上设置的自动化部署流程的uid
//	 */
//	private String deployProcessUid;
//
//	/**
//	 * api发送日志级别（feign）
//	 */
//	private Level logLevel = Level.BASIC;
//
//	public String getTenantId() {
//		return tenantId;
//	}
//
//	public void setTenantId(String tenantId) {
//		this.tenantId = tenantId;
//	}
//
//	public String getAccessId() {
//		return accessId;
//	}
//
//	public void setAccessId(String accessId) {
//		this.accessId = accessId;
//	}
//
//	public String getAccessSecret() {
//		return accessSecret;
//	}
//
//	public void setAccessSecret(String accessSecret) {
//		this.accessSecret = accessSecret;
//	}
//
//	public String getUrl() {
//		return url;
//	}
//
//	public void setUrl(String url) {
//		this.url = url;
//	}
//
//	public boolean isEnabled() {
//		return enabled;
//	}
//
//	public void setEnabled(boolean enabled) {
//		this.enabled = enabled;
//	}
//
//	/**
//	 * @return the logLevel
//	 */
//	public Level getLogLevel() {
//		return logLevel;
//	}
//
//	/**
//	 * @param logLevel the logLevel to set
//	 */
//	public void setLogLevel(Level logLevel) {
//		this.logLevel = logLevel;
//	}
//
//	/**
//	 * @return the deployProcessUid
//	 */
//	public String getDeployProcessUid() {
//		return deployProcessUid;
//	}
//
//	/**
//	 * @param deployProcessUid the deployProcessUid to set
//	 */
//	public void setDeployProcessUid(String deployProcessUid) {
//		this.deployProcessUid = deployProcessUid;
//	}
//
//}
