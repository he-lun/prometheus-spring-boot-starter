package top.codef.properties.exception;

import org.springframework.boot.context.properties.ConfigurationProperties;

import top.codef.properties.frequency.NoticeFrequencyStrategy;

@ConfigurationProperties(prefix = "prometheus.exceptionnotice.strategy")
public class ExceptionNoticeFrequencyStrategy extends NoticeFrequencyStrategy {

}
